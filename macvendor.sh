#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"         # Cela cesta ke scriptu. Kvuli datovemu souboru, kdyz se spousti odjinud.
VENDORDB="$DIR/manuf"
MAC=$1
MACV=${MAC:0:8}         # orizneme na prvnich 8 pozic zleva
MACV=${MACV^^}          # prevedeme na velka pismena
MACV=${MACV//-/:}       # nahradime vsechny pomlcky dvojteckama

VENDOR=`cat $VENDORDB | grep -m 1 $MACV |awk '{$1 = ""; $2 = ""; $3 = ""; print $0;}'`       # Aby awk ignoroval parametry 1 az 3 a cely zbytek vypise. Parametr u grep -m 1 vrati pouze prvni vyskyt.
VENDOR="${VENDOR#"${VENDOR%%[![:space:]]*}"}"
echo $VENDOR
