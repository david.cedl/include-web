<?php
function dbinit()
{
        //global $con;
        include('config/config.inc.php');
        $con = mysqli_connect($SQL_Server,$SQL_User,$SQL_Password,$SQL_Database) or die("Nelze se připojit k databázi.");
        // nastaveni kodovani cestiny
        $result = mysqli_query($con, "SET NAMES utf8");
        return $con;
}

function dbclose($con)
{
//      global $con;
        mysqli_close($con);
}

function sql_exec($sql)
{
    $spojeni = dbinit();
    $result = mysqli_query($spojeni, $sql) or die("Dotaz nelze provést: " . mysqli_error($spojeni));
    //mysqli_free_result($result);
    if(isset($result) && is_resource($result))
    {
        mysqli_free_result($result);
    }
    dbclose($spojeni);
}

function sql_exec2array($sql)
{
    $spojeni = dbinit();
    $pole = array();
    $result = mysqli_query($spojeni, $sql) or die("Dotaz nelze provést: " . mysqli_error($spojeni));
    while( $row = mysqli_fetch_assoc( $result))
    {
        $pole[] = $row; // Inside while loop
    }
    mysqli_free_result($result);
    dbclose($spojeni);
    return $pole;
}

// Vypíše formátovaně proměnnou, pole či objekt pro ladící účely.
function print_debug($promenna)
{
    echo "<pre>";
    print_r($promenna);
    echo "</pre>";
}

// Vypíše tabulku dle zadaného SQL dotazu a dle zadaného formátování.
//    Příklad:
//       $sql = "SELECT * FROM odrudy ORDER BY nazev";
//       print_table($sql, array("id","nazev","zkratka","bile","rose","cervene"), array("ID","Název","Zkratka","B","R","Č"), array("text-right","text-left","text-left","text-center","text-center","text-center"));
function print_table($sql, array $polozky, array $hlavicky, array $class_td, $class_table="table table-bordered table-hover table-sm", $class_trh="table-primary", $class_tr="table-info")
{
	$pole = sql_exec2array($sql);
        if (count($polozky) > 0)
        {
            ?>
            <table class="<?php echo $class_table;?>">
                <thead>
                    <tr class="<?php echo $class_trh;?>">
                        <?php
                        foreach ($hlavicky as $hlavicka)
                        {
                            echo "<th class=\"text-center\">$hlavicka</th>";
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($pole as $row)
                    {
                        echo "<tr class=\"$class_tr\">";
                        $i = 0;
                        foreach ($polozky as $polozka)
                        {
                            echo "<td class=\"".$class_td[$i]."\">";
                            echo $row["$polozka"];
                            echo "</td>";
                            $i++;
                        }
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <?php
        }
        else
        {
            echo "Nenalezena žádná položka.";
        }
}

function formatBytes($size, $precision = 2) {
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');
    if ($size == 0)
        return number_format(0,$precision)." ";
    else
        return number_format(round(pow(1024, $base - floor($base)), $precision),$precision) .' '. $suffixes[floor($base)];
}

function formatBits($size, $precision = 2, $logbase = 1024) {
    $base = log($size, $logbase);
    $suffixes = array('', 'K', 'M', 'G', 'T');
    if ($size == 0)
        return number_format(0,$precision)." ";
    else
        return number_format(round(pow($logbase, $base - floor($base)), $precision),$precision) .' '. $suffixes[floor($base)];
}

// Setřídění pole podle daného indexu
function array_sort($array, $on, $order=SORT_ASC)
{
    setlocale(LC_COLLATE, 'cs_CZ.utf8');
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array, SORT_LOCALE_STRING);
                //natsort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            array_push($new_array, $array[$k]);
        }
    }

    return $new_array;
}

// Vrátí věk k dnešnímu dni. Vstup je datum narození.
function getAge($date) {
    return intval(date('Y', time() - strtotime($date))) - 1970;
}

// Vrátí předchozí měsíc. Vstup i výstup ve formátu "YYYY-MM"
function rokMesicPredchozi($rokMesic)
{
    $rok=(int)substr($rokMesic, 0, 4);       // 4 znaky zleva + prevedeni na integer
    $mesic=(int)substr($rokMesic, -2, 2);    // 2 znaky zprava + prevedeni na integer
    if ($mesic==1)
    {
        $rok--;
        $mesic=12;
    }
    else
    {
        $mesic--;
    }
    return $rok . "-" . ($mesic<10 ? "0" : "") . $mesic;
}

// Vrátí jméno výrobce dle MAC adresy. ($remote=false : z lokální databáze, $remote=true : z veřejného webu CURLem)
function macvendor($mac_address, $remote=false)
{
	//  $mac_address = "FC:FB:FB:01:FA:21";
	if ($remote)
	{
		$url = "https://api.macvendors.com/" . urlencode($mac_address);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
	}
	else
	{
		$actualPath = dirname(__FILE__);
		$response = `$actualPath/macvendor.sh $mac_address`;
	}
	if($response) {
		return "$response";
	} else {
		return "Not Found";
	}
}


?>
